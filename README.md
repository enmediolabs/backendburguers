# Labs Burguers (API REST)
---
This is a API rest skeleton with swagger JSdocs

# Installation

```
git clone repo_url
cd repo
npm install
```

# Running

Default running

```bash
npm run start
```

Development Running

```bash
npm run dev
```



---
# Authors

[ Enmedio Comunicacion Digital ] [ Enmedio Labs ]
