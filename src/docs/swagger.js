const swagger = {
    definition: {
        openapi: '3.0.3',
        info: {
            title: 'Labs Burguers API',
            version: '0.0.1',
            description: 'OpenApi Documentation',
            contact: {
                name: 'Api Support',
                email: 'andres.valenzuela@enmedio.com.co'
            }
        },
        servers: [
            {
                url: 'http://localhost:' + String(process.env.PORT || 3040) + '/',
                description: 'Local'
            },
            {
                url: 'http://172.16.40.36:' + String(process.env.PORT || 3040) + '/',
                description: 'develop'
            }
        ],
        tags: [
            {
                name: 'Boucher',
                description: 'Boucher endpoints'
            },
            {
                name: 'Client',
                description: 'Client Endpoints'
            },
            {
                name: 'Product',
                description: 'Product Endpoints'
            }
        ],
        components: {
            schemas: {
                productType: {
                    type: 'object',
                    properties: {
                        type: {
                            type: 'string',
                            example: 'Ingredient'
                        }
                    }
                },
                product: {
                    type: 'object',
                    properties: {
                        name: {
                            type: 'string',
                            example: "Tomate"
                        },
                        price: {
                            type: 'string',
                            example: '1500'
                        },
                        description: {
                            type: 'string',
                            example: 'Rodajas de Tomate Seleccionado.'
                        },
                        ingredients: {
                            type: 'array',
                            items: {
                                type: 'string',
                                example: 'Tomate'
                            }
                        },
                        type: {
                            $ref: '#/components/schemas/productType/properties/type'
                        }
                    }
                },
                login: {
                    type: 'object',
                    properties: {
                        userPhone: {
                            $ref: '#/components/schemas/mobilePhone'
                        }
                    }
                },
                user: {
                    type: 'object',
                    properties: {
                        userPhone: {
                            $ref: '#/components/schemas/mobilePhone'
                        },
                        userName: {
                            type: 'string',
                            example: 'Juanito Alimaña'
                        }
                    }
                },
                mobilePhone: {
                    type: 'string',
                    example: '3009999999'
                },
                deliveryType: {
                    type: 'string',
                    example: 'for_here'
                }
            }
        }
    },
    apis: ['./src/services/**/index.js']//start in root of repo
};
module.exports = swagger;
