const normalize = require('./normalize');
const report = require('./report');

module.exports = {normalize, report};
