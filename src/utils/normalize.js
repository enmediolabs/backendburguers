/**
 *  @function
 *  normalize in capitalcase any word in a text with max size 50 chars  
 *  
 *  @name normalizeNames
 *
 *  @param {string} text
 *  
 *  @return {string} "Normalize Text"
 *  
 *  @author Cristian Murcia
 */

function normalizeNames(text) {
    if (text === undefined || text.length < 1) {
        throw {
            status: 400,
            message: 'the text can`t be empty'
        }
    }
    if (text.length > 50) {
        throw {
            status: 400,
            message: 'the text is too long that i can`t save it'
        }
    }
    text = text.toLowerCase();
    let words = text.split(' ');
    if (words.length > 1) {
        text = ''
        words.forEach((part, idx) => {
            if (idx == 0) {
                text += part.charAt(0).toUpperCase() + part.slice(1);
            } else { 
                text += ' ' + part.charAt(0).toUpperCase() + part.slice(1);
            }    
        });
    }
    text = text.charAt(0).toUpperCase() + text.slice(1);
    //console.log('normalizeName:', name);
    return text;
};

/**
 *  @function
 *  check a email string  
 *  
 *  @name checkEmail
 *
 *  @param {string} email
 *  
 *  @return {string} "normalize@text.com"
 *  
 *  @author Cristian Murcia
 */

function checkEmail(email) {
    if (email === undefined || email.length < 1) {
        throw {
            status: 400,
            message: 'The email can`t be empty'
        }
    }
    if (email.length > 50) {
        throw {
            status: 400,
            message: 'The email is too long that i can`t save it'
        }
    }
    let rex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    
    let resp = rex.test(email)
    if (!resp) {
        throw {
            status: 400,
            message: 'The email is not valid'
        }
    }

    return resp;
};


module.exports = { normalizeNames, checkEmail };
