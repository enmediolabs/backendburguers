class report {
    constructor(url, method) {
        this.url = url,
        this.method = method,
        this.start = Date.now();
    };

    time() {
        this.end = Date.now();
        let seconds = ((Date.now() - this.start) / 1000);
        this.time = `time of exec ${seconds}s`;
        return this.time
    }
}
module.exports = report;
