const conf = require('../config');
const { MongoClient, ObjectId } = require('mongodb');

let mongo = {
    client: new MongoClient(conf.dbUri),
    objID: ObjectId
};


async function checkDB() {
    try {
        await mongo.client.connect();
        console.log('[DB] ok');
    } catch (err) {
        console.log('[DB ERROR]', err);
    } finally {
        mongo.client.close();
    }
}

mongo.checkDB = checkDB;

module.exports = mongo;