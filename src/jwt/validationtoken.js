const jwt = require('jsonwebtoken');

const cfg = require('../config');
const key = cfg.tknKey;

const validUsr = async (req, res, next) => {

    let tkn = req.headers.authorization;
    if (tkn === undefined) {
        let error = {
            status: 401,
            message: "Bearer Token is required"
        }
        return res.status(error.status).json(error);
    }
    tkn = tkn.split(' ')[1];
    try {
        let token = await jwt.verify(tkn, key);
        console.log('[login user success]', token);
        req.usr = token;
    } catch (err) {
        err.status = 401;
        console.log(err);
        return res.status(401).json(err);
    }
    next();
};

const validAdm = async (req, res, next) => {
    let tkn = req.headers.authorization;
    if (tkn === undefined) {
        let error = {
            status: 401,
            message: "Bearer Token is required"
        }
        return res.status(error.status).json(error);
    }
    tkn = tkn.split(' ')[1];
    try {
        let token = await jwt.verify(tkn, key);
        if (token.aud === undefined || token.aud !== 'admin') {
            let error = {
                status: 401,
                message: 'Admin token is required'
            };
            return res.status(401).json(error);
        }
        console.log('[login succesfull]', token);
        req.usr = token;
    } catch (err) {
        err.status = 401;
        console.log(err);
        return res.status(401).json(err);
    }
    next();
};
module.exports = {validUsr, validAdm};
