const {newToken} = require('./create');
const {validUsr, validAdm} = require('./validationtoken');
module.exports = {newToken, validUsr, validAdm};
