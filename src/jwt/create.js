const jwt = require('jsonwebtoken');
const { checkEmail } = require('../utils/normalize');

const cfg = require('../config');
const priv_key = cfg.tknKey
//console.log('[priv]', priv_key);
async function newToken(issuer, payload) {
    try {
        payload.iss = issuer
        let token = await jwt.sign(payload, priv_key);
        return token;
    } catch (err) {
        throw {
            error: err,
            message: 'Generation token Error'
        }
    }

};

module.exports = {newToken};
