const { Router } = require('express');
const router = Router();
const requests = require('./request');

router.get(
    '/bouchers',
    requests.getBouchers
);
/**
 *  @swagger
 *  /bouchers:
 *      get:
 *          description:
 *              Get All Bouchers in the systems
 *          tags:
 *              - Boucher
 *          summary:
 *              Get All Bouchers
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.get(
    '/bouchers/:userPhone',
    requests.getOneBoucher
);
/**
 *  @swagger
 *  /bouchers/{userPhone}:
 *      get:
 *          description:
 *              Get One Bouchers in the systems
 *          tags:
 *              - Boucher
 *          summary:
 *              Get One Bouchers
 *          parameters:
 *              - in: path
 *                name: userPhone
 *                schema:
 *                  $ref: '#/components/schemas/mobilePhone'
 *              - in: query
 *                name: orders
 *                schema:
 *                  type: boolean
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.post(
    '/bouchers/',
    requests.postBoucher
);
/**
 *  @swagger
 *  /bouchers:
 *      post:
 *          description:
 *              Create boucher in the DB
 *          tags:
 *              - Boucher
 *          summary:
 *              Create one Boucher
 *          parameters:
 *              - in: query
 *                name: mobilePhone
 *                schema:
 *                  $ref: '#/components/schemas/mobilePhone'
 *              - in: query
 *                name: print
 *                schema:
 *                  type: boolean
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.get(
    '/print/:userPhone',
    requests.printOneBoucher
);
/**
 *  @swagger
 *  /print/{userPhone}:
 *      get:
 *          description:
 *              print voucher
 *          tags:
 *              - Boucher
 *          summary:
 *              Print voucher
 *          parameters:
 *              - in: path
 *                name: userPhone
 *                schema:
 *                  $ref: '#/components/schemas/mobilePhone'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */
module.exports = router;