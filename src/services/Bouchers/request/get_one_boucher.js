const {report} = require('../../../utils');
const {findOneBoucher} = require('../DataAccess');

const getOneBoucher = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    out.phone = req.params.userPhone;
    console.log('[paramss]', req.params.userPhone)
    try {
        out.bouchers = await findOneBoucher(out.phone);
        console.log('[query]', req.query.orders);
        if (req.query.orders === 'false')
            delete out.bouchers.order;
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = {getOneBoucher};