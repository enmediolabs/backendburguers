const {report} = require('../../../utils');
const {findAllBouchers} = require('../DataAccess');

const getBouchers = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    try {
        out.bouchers = await findAllBouchers();
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        out.error = {m: 'Not ok'};
        res.status(400).json(out);
    }
};

module.exports = {getBouchers};