const {getBouchers} = require('./get_bouchers');
const { getOneBoucher } = require('./get_one_boucher');
const {postBoucher} = require('./post_boucher');
const { printOneBoucher } = require('./print_one_boucher');
module.exports = {
    printOneBoucher,
    getOneBoucher,
    postBoucher,
    getBouchers
}