const {report} = require('../../../utils');
const { findOneUser } = require('../../Clients/DataAccess');
const {createBoucher} = require('../DataAccess');



const postBoucher = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    console.log(out);
    out.user_phone = req.body.number;
    out.print = req.query.print

    console.log('[ deliveryType ]', out.deliveryType);
    console.log('[ user phone ]', out.user_phone);

    console.log('[Query]', req.query);
    console.log('[params]', req.params);
    //console.log('[BODY]', req.body);
    out.order = req.body.order;
    out.no_order = req.body.numberOrder;
    out.total = req.body.total;
    out.type = req.body.type;
    try {
        let {name, phone} = await findOneUser(out.user_phone);
        out.user = {name, phone};
        
    } catch (err) {
        out.time();
        console.log(`[post voucher getting user Error]`, err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
    try {
        let boucher = {
            user: out.user, 
            delivery: out.deliveryType, 
            order: out.order, 
            no_order: out.no_order,
            total: out.total,
            type: out.type
        };
        out.ticket = await createBoucher(boucher);
        console.log('[Created voucher]', out.ticket);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log(`[post voucher creating voucher error]`, err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = {postBoucher};