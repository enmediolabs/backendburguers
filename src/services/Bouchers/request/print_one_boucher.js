const {report} = require('../../../utils');
const {findOneBoucher} = require('../DataAccess');
const {printBoucher} = require('../printer');

const printOneBoucher = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    console.log(out);
    out.phone = req.params.userPhone;
    console.log('[paramss]', req.params.userPhone);
    try {
        console.log('[print voucher | phone]', out.phone);
        out.bouchers = await findOneBoucher(out.phone);
        for (var idx = 0; idx < out.bouchers.order.length; idx++) {
            delete out.bouchers.order[idx].image
            delete out.bouchers.order[idx].selected
            delete out.bouchers.order[idx].styles
            delete out.bouchers.order[idx].id
            delete out.bouchers.order[idx].typeProduct
            console.log('[CLEAN (-_-) ]', out.bouchers.order[idx]);
        }
        let boucher = {
            user: out.bouchers.user,
            status: out.bouchers.status,
            order: out.bouchers.order,
            total: out.bouchers.total
        };
        console.log('[Print ?]', boucher);
        //if (out.print === 'patrañas')
        printBoucher(boucher);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[print one voucher error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = {printOneBoucher};