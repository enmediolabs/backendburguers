const {client, objID} = require('../../../db/mongo_db');

async function createBoucher(baseBoucher) {

    baseBoucher.create_at = Date.now();
    baseBoucher.status = 'in_progress'
    baseBoucher.statusOrder = 0;
    try {
        await client.connect();

        const lastboucher = await client
        .db('labs_burguers')
        .collection('bouchers')
        .find({})
        .count();

        console.log('[boucher count]',lastboucher)
        baseBoucher.no_order = (lastboucher + 1);
        const result = await client
        .db('labs_burguers')
        .collection('bouchers')
        .insertOne(baseBoucher);

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any boucher',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if  (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllBouchers}: ${String(err)}` 
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = {createBoucher};