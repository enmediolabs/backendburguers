const {findAllBouchers} = require('./find_all_bouchers');
const {createBoucher} = require('./create_boucher');
const { findOneBoucher } = require('./find_one_boucher');
module.exports = {
    createBoucher,
    findAllBouchers,
    findOneBoucher
}