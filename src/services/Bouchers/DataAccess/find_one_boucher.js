const {client, objID} = require('../../../db/mongo_db');

async function findOneBoucher(phone) {
    try {
        await client.connect();
        console.log('[phone]', phone)

        const result = await client
        .db('labs_burguers')
        .collection('bouchers')
        .findOne({
            'user.phone': phone
        },
        {
            sort: {'create_at': -1}
        });
        console.log('[boucher ]', result);
        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any boucher',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if  (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllBouchers}: ${String(err)}` 
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = {findOneBoucher};