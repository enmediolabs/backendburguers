const printDriver = require('@thiagoelg/node-printer');
const ThermalPrinter = require("node-thermal-printer").printer;
const PrinterTypes = require("node-thermal-printer").types;

let def = printDriver.getPrinter();

console.log('[default printer]', def)
 
let printer = new ThermalPrinter(
    {
        type: PrinterTypes.EPSON,
        interface: `printer:${def.name}`,
        driver: printDriver,
        options:{
            timeout: 5000
        }
});

async function checkMachine (test) {
    let flag = await printer.isPrinterConnected();
    if (test) {
        printer.printQR("Enmedio Labs - sirve", {
            cellSize: 8,             // 1 - 8
            correction: 'M',         // L(7%), M(15%), Q(25%), H(30%)
            model: 2                 // 1 - Model 1
                                     // 2 - Model 2 (standard)
                                     // 3 - Micro QR
        });
        printer.cut();
    }
    await printer.execute();
    console.log('[Print Connection]', flag);
    return flag;
}

async function printBoucher (boucher) {
    console.log('[Boucher]', boucher);
    printer.alignCenter();
    await printer.printImage('./src/services/Bouchers/printer/logo.png');
    printer.alignLeft();
    let ueue = [];
    let separator = '########################################'
    ueue.push(separator);
    ueue.push(String(Date()).substring(40, 0));
    ueue.push(String(Date()).substring(40));
    ueue.push(separator);
    let user = `User: ${boucher.user.name} - Tel: ${boucher.user.phone}`
    if (user.length > 42) {
        user = user.split('-')
        ueue.push(user[0]);
        ueue.push(user[1]);
    } else {
        ueue.push(user);
    }
    ueue.push(`Estado de la orden: ${boucher.status}`);
    ueue.push(separator);
    for (var idx = 0; idx < boucher.order.length; idx++) {
        let products = `${boucher.order[idx].title}\t${boucher.order[idx].priceReference}\t${boucher.order[idx].cuantity}\t${boucher.order[idx].price} \n`;
        ueue.push(products);
    }
    ueue.push(separator);
    ueue.push(`\n Total a pagar: ${boucher.total}\n`);
    ueue.push(separator);
    ueue.push('Powered by: Enmedio Labs');
    for (var idx = 0; idx < ueue.length; idx++) {
        console.log(ueue[idx]);
        printer.println(ueue[idx]);
    }
    //console.log(ueue);
    /*
    printer.println('############################\n');
    printer.println(Date());
    printer.println('############################\n');
    printer.println(`User: ${boucher.user.name} - Tel: ${boucher.user.phone}`)
    printer.println(`Estado de la orden: ${boucher.status}`);
    printer.println('############################\n');
    for (var idx = 0; idx < boucher.order.length; idx++) {
        printer.println(`${boucher.order[idx].title}\t${boucher.order[idx].priceReference}\t${boucher.order[idx].cuantity}\t${boucher.order[idx].price} \n`);
        //console.log(`${boucher.order[idx].title}\n${boucher.order[idx].priceReference}\t${boucher.order[idx].cuantity}\t${boucher.order[idx].price} \n`);
    }
    printer.println('############################\n');
    printer.println(`\nTotal a pagar: ${boucher.total}\n`);
    printer.println('############################\n');
    printer.println(`Powered by: Enmedio Labs`);
    */
    printer.cut();
    await printer.execute();
    printer.clear();
}

module.exports = {
    checkMachine,
    printBoucher
}
