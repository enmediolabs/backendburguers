const { report } = require('../../../utils');
const { findType } = require('../DataAccess');

const getType = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    out.type = req.params.type;
    try {
        out.product_type = await findType(out.type);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { getType };