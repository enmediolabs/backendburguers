const { report } = require('../../../utils');
const { findProduct } = require('../DataAccess');

const getProduct = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    out.name_product = req.params.name_product;
    console.log('[name]', out.name_product);
    try {
        out.products = await findProduct(out.name_product);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { getProduct };