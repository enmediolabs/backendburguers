const { report } = require('../../../utils');
const { findProductByType } = require('../DataAccess');

const getProductByType = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    out.type_product = req.params.type;
    console.log('[type]', out.type_product);
    try {
        out.products = await findProductByType(out.type_product);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { getProductByType };