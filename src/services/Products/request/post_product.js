const { report } = require('../../../utils');
const { createProduct, findType, findProduct } = require('../DataAccess');

const postProduct = async (req, res) => {
    let out = new report(req.originalUrl, req.method);

    let { name, price, description, ingredients, type } = req.body;
    out.product = { name, price, description, ingredients, type };
    try {
        await findType(out.product.type);
        switch (out.product.type) {
            case 'Drink':
            case 'Ingredient':
                delete out.product.ingredients
                break;
            case 'Product':
                for (var idx = 0; idx < out.product.ingredients.length; idx++) {
                    await findProduct(out.product.ingredients[idx]);
                }
        }
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
        out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
    console.log('[ product ]', out.product);
    try {
        out.product = createProduct(out.product);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { postProduct };