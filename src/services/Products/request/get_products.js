const { report } = require('../../../utils');
const { findAllProducts } = require('../DataAccess');

const getProducts = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    try {
        out.products = await findAllProducts();
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { getProducts };