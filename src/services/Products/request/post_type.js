const { report } = require('../../../utils');
const { createType } = require('../DataAccess');

const postType = async (req, res) => {
    let out = new report(req.originalUrl, req.method);

    let { type } = req.body;
    out.productType = { type };
    console.log('[ product type ]', out.productType);
    try {
        out.product = createType(out.productType);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { postType };