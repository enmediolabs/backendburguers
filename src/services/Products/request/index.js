// cosnt {} = require();
const { getProducts } = require('./get_products');
const { postProduct } = require('./post_product');
const { getTypes } = require('./get_types');
const { postType } = require('./post_type');
const { getType } = require('./get_type');
const { getProduct } = require('./get_product');
const { getProductByType } = require('./get_products_type');

module.exports = {
    getProductByType,
    getProduct,
    getType,
    postType,
    getTypes,
    postProduct,
    getProducts
}