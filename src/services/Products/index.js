const { Router } = require('express');
const router = Router();
const requests = require('./request');

router.get(
    '/product-types',
    requests.getTypes
);
/**
 *  @swagger
 *  /product-types:
 *      get:
 *          description:
 *              Get All types aviable for a product
 *          tags:
 *              - Product
 *          summary:
 *              Get All types aviable for a product
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.get(
    '/product-types/:type',
    requests.getType
);
/**
 *  @swagger
 *  /product-types/{type}:
 *      get:
 *          description:
 *              Get One type aviable for a product
 *          tags:
 *              - Product
 *          summary:
 *              Get One type aviable for a product
 *          parameters:
 *              - in: path
 *                name: type
 *                schema:
 *                  $ref: '#/components/schemas/productType/properties/type'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.post(
    '/product-types',
    requests.postType
);
/**
 *  @swagger
 *  /product-types:
 *      post:
 *          description:
 *              Create type of product
 *          tags:
 *              - Product
 *          summary:
 *              Create one type of product
 *          requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/productType'
 *                  application/x-www-form-urlencoded:
 *                      schema:
 *                          $ref: '#/components/schemas/productType'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */


router.get(
    '/products',
    requests.getProducts
);
/**
 *  @swagger
 *  /products:
 *      get:
 *          description:
 *              Get All products in the system
 *          tags:
 *              - Product
 *          summary:
 *              Get All Products
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

 router.get(
    '/products/:type',
    requests.getProductByType
);
/**
 *  @swagger
 *  /products/{type}:
 *      get:
 *          description:
 *              Get All products in the system
 *          tags:
 *              - Product
 *          summary:
 *              Get All Products
 *          parameters:
 *              - in: path
 *                name: type
 *                schema:
 *                  $ref: '#/components/schemas/productType/properties/type'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

 router.get(
    '/product/:name_product',
    requests.getProduct
);
/**
 *  @swagger
 *  /product/{name_product}:
 *      get:
 *          description:
 *              Get All products in the system
 *          tags:
 *              - Product
 *          summary:
 *              Get All Products
 *          parameters:
 *              - in: path
 *                name: name_product
 *                schema:
 *                  $ref: '#/components/schemas/product/properties/name'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.post(
    '/product',
    requests.postProduct
);
/**
 *  @swagger
 *  /product:
 *      post:
 *          description:
 *              Create product in the DB
 *          tags:
 *              - Product
 *          summary:
 *              Create one product
 *          requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/product'
 *                  application/x-www-form-urlencoded:
 *                      schema:
 *                          $ref: '#/components/schemas/product'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */
module.exports = router;