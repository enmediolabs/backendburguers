const { client, objID } = require('../../../db/mongo_db');

async function findProduct(name_product) {
    try {
        await client.connect();

        const result = await client
            .db('labs_burguers')
            .collection('products')
            .findOne({"name": name_product});

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any product with this name',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllProducts}: ${String(err)}`
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = { findProduct };