const { client, objID } = require('../../../db/mongo_db');

async function findProductByType(type_product) {
    try {
        await client.connect();

        console.log('[type]', type_product)
        const result = await client
            .db('labs_burguers')
            .collection('products')
            .find({"type": type_product})
            .toArray();

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any product with this name',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllProducts}: ${String(err)}`
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = { findProductByType };