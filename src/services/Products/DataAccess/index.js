// const {} = require('');
const { findAllProducts } = require('./find_all_products');
const { createProduct } = require('./create_product');
const { findAllTypes } = require('./find_all_types');
const { createType } = require('./create_type');
const { findType } = require('./find_type');
const { findProduct } = require('./find_product');
const { findProductByType } = require('./find_products_type');

module.exports = {
    findProductByType,
    findProduct,
    findType,
    createType,
    findAllTypes,
    createProduct,
    findAllProducts
}