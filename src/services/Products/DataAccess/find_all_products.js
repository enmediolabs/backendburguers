const { client, objID } = require('../../../db/mongo_db');

async function findAllProducts() {
    try {
        await client.connect();

        const result = await client
            .db('labs_burguers')
            .collection('products')
            .find({})
            .toArray();

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any product',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllProducts}: ${String(err)}`
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = { findAllProducts };