const { client, objID } = require('../../../db/mongo_db');

async function createProduct(baseProduct) {

    baseProduct.create_at = Date.now();
    try {
        await client.connect();

        const result = await client
            .db('labs_burguers')
            .collection('products')
            .insertOne(baseProduct);

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any product',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{createProduct}: ${String(err)}`
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = { createProduct };