const { client, objID } = require('../../../db/mongo_db');

async function findType(type) {
    try {
        await client.connect();

        const result = await client
            .db('labs_burguers')
            .collection('types')
            .findOne({'type': type});

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any type of product',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllProducts}: ${String(err)}`
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = { findType };