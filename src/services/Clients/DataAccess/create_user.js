const { client, objID } = require('../../../db/mongo_db');

async function createUser(user) {

    user.create_at = Date.now();
    user.status = 1;
    try {
        await client.connect();

        const result = await client
            .db('labs_burguers')
            .collection('users')
            .insertOne(user);

        /*
        await client
        .db('labs_burguers')
        .collection('users')
        .createIndex({'phone': 1}, {unique: true});*/
        console.log('[result]', result)
        return result;
    } catch (err) {
        if (err.code === 11000) {
            throw {
                status: 409,
                message: 'This user already exist'
            }
        } else if (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllBouchers}: ${String(err)}`
            }
        }
    } finally {
        await client.close();
    }
};

module.exports = { createUser };