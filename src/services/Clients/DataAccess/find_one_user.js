const {client, objID} = require('../../../db/mongo_db');

async function findOneUser(mobilePhone) {
    try {
        await client.connect();

        const result = await client
        .db('labs_burguers')
        .collection('users')
        .findOne({'phone': mobilePhone});

        if (!result || result === undefined) {
            throw {
                status: 404,
                message: 'Not Found any user',
                code: 'xxx1'
            }
        }
        return result;
    } catch (err) {
        if  (err.status === undefined) {
            throw {
                status: 500,
                message: `[Internal DB Error]-{findAllUsers}: ${String(err)}` 
            }
        }
        throw err;
    } finally {
        await client.close();
    }
};

module.exports = {findOneUser};