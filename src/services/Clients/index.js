const { Router } = require('express');
const router = Router();
const requests = require('./request');

router.get(
    '/clients',
    requests.getUsers
);
/**
 *  @swagger
 *  /clients:
 *      get:
 *          description:
 *              Get All users in the system
 *          tags:
 *              - Client
 *          summary:
 *              Get All users in the system
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */


router.get(
    '/clients/:user_phone',
    requests.getOneUser
);

/**
 *  @swagger
 *  /clients/{user_phone}:
 *      get:
 *          description:
 *              Get one user in the system by mobile phone
 *          tags:
 *              - Client
 *          summary:
 *              Get one user in the system by mobile phone
 *          parameters:
 *              - in: path
 *                name: user_phone
 *                schema:
 *                  $ref: '#/components/schemas/mobilePhone'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

 router.delete(
    '/clients/:user_phone',
    requests.deleteOneUser
);

/**
 *  @swagger
 *  /clients/{user_phone}:
 *      delete:
 *          description:
 *              Delete one user in the system by mobile phone
 *          tags:
 *              - Client
 *          summary:
 *              Delete one user in the system by mobile phone
 *          parameters:
 *              - in: path
 *                name: user_phone
 *                schema:
 *                  $ref: '#/components/schemas/mobilePhone'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

router.post(
    '/client',
    requests.postUser
);
/**
 *  @swagger
 *  /client:
 *      post:
 *          description:
 *              Create user
 *          tags:
 *              - Client
 *          summary:
 *              Create one user
 *          requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/user'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */

 router.post(
    '/login',
    requests.login
);
/**
 *  @swagger
 *  /login:
 *      post:
 *          description:
 *              login user
 *          tags:
 *              - Client
 *          summary:
 *              login user
 *          requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/login'
 *          responses:
 *              200:
 *                  description: OK
 *              400:
 *                  description: Not OK
 */
module.exports = router;