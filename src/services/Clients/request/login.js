const { report } = require('../../../utils');
const { findOneUser } = require('../DataAccess');
const { newToken } = require('../../../jwt');

const login = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    out.userPhone = req.body.userPhone;
    console.log('[user phone]', out.userPhone);
    try {
        out.user = await findOneUser(out.userPhone);
        console.log('[user]', out.user);

        try {
            let token = await newToken(out.user.name, {
                aud: "client",
                exp: (Math.floor(Date.now() / 1000) + (60 * 30) * 1) //(s * M) * H
            });
            out.token = token;
        } catch (err) {
            out.time();
            out.error = err;
            return res.status(500).json(out);
        }
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { login };