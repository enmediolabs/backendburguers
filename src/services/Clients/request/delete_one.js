const { report } = require('../../../utils');
const { DeleteOneUser } = require('../DataAccess');

const deleteOneUser = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    out.userPhone = req.params.user_phone;
    console.log('[user phone]', out.userPhone);
    try {
        out.user = await DeleteOneUser(out.userPhone);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = { deleteOneUser };