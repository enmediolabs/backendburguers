const {report} = require('../../../utils');
const {createUser} = require('../DataAccess');

const postUser = async (req, res) => {
    let out = new report(req.originalUrl, req.method);

    console.log('[body]', req.body);
    out.user_phone = req.body.userPhone;
    out.user_name = req.body.userName;

    console.log('[ User Name ]', out.user_name);
    console.log('[ user phone ]', out.user_phone);
    try {
        let user = {name: out.user_name, phone: out.user_phone};
        out.user = await createUser(user);
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        console.log('[Error]', err);
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = {postUser};