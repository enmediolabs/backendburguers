const {report} = require('../../../utils');
const {findAllUsers} = require('../DataAccess');

const getUsers = async (req, res) => {
    let out = new report(req.originalUrl, req.method);
    try {
        out.users = await findAllUsers();
        out.time();
        res.status(200).json(out);
    } catch (err) {
        out.time();
        if (err.status !== undefined)
            out.error = err;
        else {
            out.error = {
                message: string(err),
                status: 500
            };
        }
        return res.status(out.error.status).json(out);
    }
};

module.exports = {getUsers};