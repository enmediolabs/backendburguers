const { getUsers } = require('./get_users');
const { postUser } = require('./post_user');
const { getOneUser } = require('./get_one_user');
const {login} = require('./login');
const { deleteOneUser } = require('./delete_one');
module.exports = {
    deleteOneUser,
    login,
    getOneUser,
    postUser,
    getUsers
}