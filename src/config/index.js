require('dotenv').config();
module.exports = {
    dbUri: process.env.DBURI,
    tknKey: process.env.TOKENKEY
};