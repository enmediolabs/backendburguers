const express = require('express');
const cors = require('cors');
require('dotenv').config();

const app = express();
app.use(cors());

const swagger = require('swagger-ui-express');
const SJSDocs = require('swagger-jsdoc');
const openAPIFile = require('./docs/swagger');
const { error404, error5xx } = require("./middlewares/errorHandler");
const routes = require('./routes');

app.use(express.urlencoded({extended: false}));
app.use(express.json());

const docs = SJSDocs(openAPIFile);
const SGopt = {explorer: false};

require('./db/mongo_db').checkDB();

app.use(routes);
app.use('/docs', swagger.serve, swagger.setup(docs, SGopt));

app.get('/', (req, res) => {
    console.log('[root]');
    res.redirect('/docs');
});

app.use(error404);
app.use(error5xx);

module.exports = app;