const {report} = require('../utils/');
const error404 = (request, response) => {
    let out = new report(request.originalUrl, request.method);
    out.error = {type: 404, message: 'Not Found'};
    out.time(); 
    response.status(out.error.type).json(out);
};

const error5xx = (error, request, response) => {
    let report = {
        url: request.originalUrl,
        method: request.method,
        code: error.status || 500,
        message: error.message || String(error),
    };
    response.status(report.code).json(report);
};

module.exports = {error404, error5xx};
