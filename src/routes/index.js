const express = require('express');
const router = express.Router();

//const service = require('service path');
//router.use('/', service);

router.use('/test', (req, res) => {
    console.log('[TEST]');
    res.status(200).json({ m: 'OK' });
});

const bouchers = require('../services/Bouchers');
router.use('/', bouchers);
const clients = require('../services/Clients');
router.use('/', clients);
const products = require('../services/Products');
router.use('/', products);

module.exports = router;
